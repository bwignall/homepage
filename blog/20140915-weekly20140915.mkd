---
author: thoughtpolice
title: "GHC Weekly News - 2014/09/15"
date: 2014-09-15
tags: ghc news
---

Hi \*,

Here's a new thing: Blog posts! That's right. A while back, we started a new
set of emails on the developers list containing weekly updates, from GHC HQ.
But we eventually decided it should be more broad and encompass the work GHC
sees as a project - including all the things our contributors do.

So now it's the weekly GHC news - and we (or, well, I) have decided to blogify
the weekly emails!

So without further adieu, here's the current recap. The original mailing list
copy is available
[here](https://www.haskell.org/pipermail/ghc-devs/2014-September/006309.html).

 - As Gabor mentioned on the list earlier today, I (Austin) accidentally broke
   the Windows build. Sorry. :( We really need to get Phab building Windows too
   ASAP... I'm working on a fix this morning.

 - I sent out the HCAR draft this morning. Please edit it! I think we have a
   few weeks of lead time however, so we're not in a rush like last time. But I
   will send reminders. :)

 - The server migration for ghc.haskell.org seems to have gone pretty smoothly
   in the past week. It's had plenty of traffic so far. The full migration is
   still ongoing and I want to complete it this week.

 - I've finished reorganizing some of the Git and Repository pages after some
   discussion. We now have the Repositories [1][] page, linked to on the left
   side, which details some notes on the repositories we use, and links to other
   various pages. I'm thinking of replacing this side-bar "root" with a link to
   the main Git [2][] page, perhaps.

 - Miscellaneous: ghc.haskell.org and phabricator.haskell.org now sets the
   `Strict-Transport-Security` header. This just means you always use SSL now
   when you visit those pages (so you can't be connection-hijacked via a 503
   redirect).

 - I'm merging some patches at the moment, although the windows fix is
   currently what I'll push first: Phab:D205, Phab:D204, Phab:D203, Phab:D199,
   Phab:D194, Phab:D179, Phab:D174. Do let me know if there's anything you want
   me to look at.

 - GHC works on Wine apparently for all you Linux users - thanks Mikolaj! [3][]

 - Jan had some questions about infrastructure which I just followed up on this
   morning. In particular: does anyone feel strongly about his first question? [4][]

 - Herbert committed the first part of the Traversable/Foldable changes, by
   moving the typeclasses to Prelude. This is part of an ongoing series of
   patches. Things like adding Bifunctor will finally come after this. [5][]

Also, added bonus: we'll start including some of the tickets we closed
this week.

Closed tickets for the past week include: #9585, #9545, #9581, #6086, #9558, and #3658.

Please let me know if you have any questions.

[1]: https://ghc.haskell.org/trac/ghc/wiki/Repositories
[2]: https://ghc.haskell.org/trac/ghc/wiki/WorkingConventions/Git
[3]: https://www.haskell.org/pipermail/ghc-devs/2014-September/006283.html
[4]: https://www.haskell.org/pipermail/ghc-devs/2014-September/006275.html
[5]: https://phabricator.haskell.org/D209
